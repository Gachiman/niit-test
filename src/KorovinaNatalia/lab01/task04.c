#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <math.h>


main()

{
	int ch, iFHeight = 0, iIHeight = 0 ;
	float fSmHeight=0;


	while (1)
	{
		printf("Enter your height in ft and inches, using format xx'xx\", please \n");

		if (scanf("%d\'%d", &iFHeight, &iIHeight) == 2)
		{
			fSmHeight = 2.54*(iFHeight * 12.0 + iIHeight);

			printf("Your height in centimeters is: %0.1f\n.", fSmHeight);

			break;
		}

		else
		{
			puts("Input error!");
			do
				ch = getchar();
			while (ch != '\n'&& ch != EOF); //EOF - buffer is clear

		}

	}
	return 0;


}
